resource "aws_dynamodb_table" "dynamodb-table" {
  name           = "${var.name_prefix}-table"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "MessageId"
  range_key      = "Message"

  attribute {
    name = "MessageId"
    type = "S"
  }

  attribute {
    name = "Message"
    type = "S"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }

  tags = {
    "DevOps"     = var.project_tag
  }
}