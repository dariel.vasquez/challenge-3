variable "project_tag" {}

variable "name_prefix" {}

variable "ecs_image" {}

variable "private_subnet" {}

variable "public_subnet" {}

variable "security_group" {}

variable "execution_role" {}

variable "target_group" {}

variable "alb_listener" {}

variable "cloudwatch_log" {}

variable "secretmanager_arn" {}