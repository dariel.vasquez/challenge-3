resource "aws_ecs_cluster" "cluster" {
  name = "${var.name_prefix}-ecs-cluster"
  setting {
    name  = "containerInsights"
    value = "enabled"
  }
  tags = {
    "DevOps"     = var.project_tag
  }
}

resource "aws_ecs_task_definition" "task" {
  family                   = "${var.name_prefix}-ecs-task"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn       = var.execution_role
  cpu                      = 512
  memory                   = 1024

  container_definitions = jsonencode([{
    name      = "${var.name_prefix}-app-container"
    image     = var.ecs_image
    cpu       = 256
    memory    = 512
    essential = true
    portMappings = [
      {
        containerPort = 80
        hostPort      = 80
      }
    ]
    environment = [
      {
        name  = "accessKeyId",
        value = jsondecode(data.aws_secretsmanager_secret_version.current.secret_string)["accessKeyId"]
      },
      {
        name  = "secretAccessKey",
        value = jsondecode(data.aws_secretsmanager_secret_version.current.secret_string)["secretAccessKey"]
      }
    ]
    logConfiguration = {
      logDriver = "awslogs"
      options = {
        "awslogs-group"         = var.cloudwatch_log
        "awslogs-region"        = "us-east-1"
        "awslogs-stream-prefix" = "ecs"
      }
    }
  }])

  tags = {
    "DevOps"     = var.project_tag
  }
}

resource "aws_ecs_service" "app_service" {
  name            = "${var.name_prefix}-app-service"
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.task.arn
  launch_type     = "FARGATE"
  desired_count   = 1
  network_configuration {
    subnets          = [var.private_subnet]
    security_groups  = [var.security_group]
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = var.target_group
    container_name   = "${var.name_prefix}-app-container"
    container_port   = 80
  }

  tags = {
    "DevOps"     = var.project_tag
  }
}

data "aws_secretsmanager_secret" "secret" {
  arn = var.secretmanager_arn
}

data "aws_secretsmanager_secret_version" "current" {
  secret_id     = data.aws_secretsmanager_secret.secret.id
  version_stage = "AWSCURRENT"
}