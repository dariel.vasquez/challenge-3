resource "aws_launch_template" "ecs_launch_template" {
  name_prefix            = "${var.name_prefix}-ecs-launch-"
  image_id               = var.ami_image
  instance_type          = "t2.micro"
  # key_name               = aws_key_pair.keypair.key_name
  vpc_security_group_ids = [var.security_group]
  user_data = base64encode(templatefile("${path.module}/ecs_config.sh", {
    LOG_FILE="/var/log/user_data.log"
    ECS_CLUSTER = var.name_prefix
  }))

  iam_instance_profile {
    name = "ecsInstanceRole"
  }

  tags = {
    "DevOps"     = var.project_tag
  }
}

resource "aws_autoscaling_group" "ecs_autoscaling_group" {
  name              = "${var.name_prefix}_ecs_autoscaling_group"
  max_size          = 3
  min_size          = 2
  desired_capacity  = 2
  health_check_type = "EC2"
  launch_template {
    id      = aws_launch_template.ecs_launch_template.id
    version = aws_launch_template.ecs_launch_template.latest_version
  }
  vpc_zone_identifier = [var.public_subnet, var.private_subnet]
  tag {
    key                 = "Name"
    value               = "${var.name_prefix}_ecs_autoscaling_group"
    propagate_at_launch = true
  }
  # target_group_arns   = [aws_alb_target_group.target_group.arn]
}

