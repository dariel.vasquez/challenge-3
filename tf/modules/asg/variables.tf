variable "project_tag" {}

variable "name_prefix" {}

variable "ami_image" {}

variable "security_group" {}

variable "public_subnet" {}

variable "private_subnet" {}