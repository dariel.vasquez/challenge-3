resource "aws_lb" "load_balancer" {
  name               = "${var.name_prefix}-load-balancer"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.security_group]
  subnets            = [var.private_subnet, var.public_subnet]
  tags = {
    Name = "${var.name_prefix}_load_balancer"
    "DevOps"     = var.project_tag
  }
}

resource "aws_lb_target_group" "target_group" {
  name        = "${var.name_prefix}-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc
  target_type = "ip"
  tags = {
    Name = "${var.name_prefix}_target_group"
    "DevOps"     = var.project_tag
  }
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.load_balancer.arn
  port              = 443
  protocol          = "HTTPS"
  certificate_arn = var.cloudfront_certificate_arn
  ssl_policy        = "ELBSecurityPolicy-2016-08"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn
  }
}