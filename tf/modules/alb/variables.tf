variable "project_tag" {}

variable "name_prefix" {}

variable "private_subnet" {}

variable "public_subnet" {}

variable "security_group" {}

variable "vpc" {}

variable "cloudfront_certificate_arn" {}