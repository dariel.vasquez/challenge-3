resource "aws_cloudwatch_log_group" "cloudwatch_log" {
  name = "${var.name_prefix}-cloudwatch-log"

  tags = {
    "DevOps"     = var.project_tag
  }
}