variable "name_prefix" {}

variable "project_tag" {}

variable "bucket_website_endpoint" {}

variable "cloudfront_certificate_arn" {}

variable "hosted_zone_name" {}