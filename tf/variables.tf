variable "access_key" {}

variable "secret_key" {}

variable "aws_region" {}

variable "aws_profile" {}

variable "project_tag" {}

variable "name_prefix" {}

variable "ecs_image" {}

variable "ami_image" {}

variable "execution_role" {}

variable "secretmanager_arn" {}

variable "hosted_zone_name" {}

variable "server_hosted_zone_name" {}

variable "cloudfront_certificate_arn" {}

variable "zone_id" {}