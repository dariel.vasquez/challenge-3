provider "aws" {
  region = "us-east-1"
  profile = var.aws_profile
}

# terraform {
#   required_providers {
#     aws = {
#       source  = "hashicorp/aws"
#       version = "~> 5.0"
#     }
#   }
# }

module "vpc" {
  source = "./modules/vpc"
  project_tag = var.project_tag
  name_prefix = var.name_prefix
}

module "asg" {
  source = "./modules/asg"
  ami_image = var.ami_image
  project_tag = var.project_tag
  name_prefix = var.name_prefix
  public_subnet = module.vpc.public_subnet
  private_subnet = module.vpc.private_subnet
  security_group = module.security_group.security_group
}

# module "ecr" {
#   source = "./modules/ecr"
#   project_tag = var.project_tag
#   name_prefix = var.name_prefix
# }

module "ecs" {
  source = "./modules/ecs"
  ecs_image = var.ecs_image
  project_tag = var.project_tag
  name_prefix = var.name_prefix
  public_subnet = module.vpc.public_subnet
  private_subnet = module.vpc.private_subnet
  security_group = module.security_group.security_group
  execution_role = var.execution_role
  target_group = module.alb.target_group
  alb_listener = module.alb.alb_listener
  cloudwatch_log = module.cloudwatch.cloudwatch_log
  secretmanager_arn = var.secretmanager_arn
}

module "security_group" {
  source = "./modules/security_group"
  project_tag = var.project_tag
  name_prefix = var.name_prefix
  vpc = module.vpc.vpc
}

module "alb" {
  source = "./modules/alb"
  project_tag = var.project_tag
  name_prefix = var.name_prefix
  vpc = module.vpc.vpc
  private_subnet = module.vpc.private_subnet
  public_subnet = module.vpc.public_subnet
  security_group = module.security_group.security_group
  cloudfront_certificate_arn = var.cloudfront_certificate_arn
}

module "cloudwatch" {
  source = "./modules/cloudwatch"
  project_tag = var.project_tag
  name_prefix = var.name_prefix
  ecs_cluster = module.ecs.ecs_cluster
  ecs_service = module.ecs.ecs_service
}

module "s3" {
  source = "./modules/s3"
  project_tag = var.project_tag
  name_prefix = var.name_prefix  
}

module "dynamoDB" {
  source = "./modules/dynamoDB"
  project_tag = var.project_tag
  name_prefix = var.name_prefix    
}

module "cloudfront" {
  source = "./modules/cloudfront"
  project_tag = var.project_tag
  name_prefix = var.name_prefix
  bucket_website_endpoint = module.s3.bucket_website_endpoint
  cloudfront_certificate_arn = var.cloudfront_certificate_arn
  hosted_zone_name = var.hosted_zone_name
}

module "route53" {
  source = "./modules/route53"
  project_tag = var.project_tag
  name_prefix = var.name_prefix
  hosted_zone_name = var.hosted_zone_name
  zone_id = var.zone_id
  resource_domain_name = module.cloudfront.s3_frontend_distribution.domain_name
  resource_hosted_zone = module.cloudfront.s3_frontend_distribution.hosted_zone_id
}

module "server_route53" {
  source = "./modules/route53"
  project_tag = var.project_tag
  name_prefix = var.name_prefix
  hosted_zone_name = var.server_hosted_zone_name
  zone_id = var.zone_id
  resource_domain_name = module.alb.alb_dns_name
  resource_hosted_zone = module.alb.alb_zone_id
}