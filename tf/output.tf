output "alb_dns_name" {
  value = module.alb.alb_dns_name
}

output "bucket_website_endpoint" {
  value = module.s3.bucket_website_endpoint
}