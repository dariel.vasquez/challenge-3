FROM node:20-alpine

WORKDIR /home/node/app

COPY /server/package*.json .

RUN apk update && npm install

COPY /server/index.js .

EXPOSE 80

CMD ["npm", "start"]

