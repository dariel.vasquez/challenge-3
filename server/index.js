const express = require("express");
const cors = require("cors");
const AWS = require("aws-sdk");
const { v4: uuidv4 } = require("uuid");

const app = express();
const port = 80;

AWS.config.update({
  region: "us-east-1",
  accessKeyId: process.env.accessKeyId,
  secretAccessKey: process.env.secretAccessKey,
});
const dynamodb = new AWS.DynamoDB();

app.use(express.json());

app.use(cors());

app.get("/", (req, res) => {
  const message = "Hello, World!";
  res.send(message);
});

app.get("/message", (req, res) => {
  const message = "You're reading a message!";
  res.send(message);
});

app.post("/api/message", async (req, res) => {
  const { message } = req.body;

  const messageId = uuidv4();

  const params = {
    TableName: "alejandro-project-table",
    Item: {
      MessageId: { S: messageId },
      Message: { S: message },
    },
  };

  console.log("Received message:", message);

  try {
    await dynamodb.putItem(params).promise();
    res
      .status(200)
      .json({ success: true, message: "Message saved to DynamoDB" });
  } catch (error) {
    console.error("Error saving message to DynamoDB:", error);
    res.status(500).json({ success: false, message: "Internal Server Error" });
  }
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
