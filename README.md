# Instructions

Remember to do all deployments using terraform. Tag all terraform resources with your name so that you can keep track of them for deletion after you finish the project.

1. Create a nodejs application that returns a message from an endpoint. Use the express npm package for this or any package you prefer
2. Dockerize this application.
3. Store the docker image in Acklen’s AWS ECR service.
4. Deploy the application in an EC2 instance. Make sure to place the EC2 instance in a private network. USE ECS FOR THIS PART
5. Create a very simple front end application that sends a request to the backend. Deploy it using AWS S3
6. Modify the backend application so that it receives the message from the front end and prints it out as a log. Deploy the modified backend.
7. Find the logs using AWS CloudWatch.
8. Create and Deploy a DB using any AWS DB service that stores the message sent from the front end. Make sure the DB only contacts the backend.
9. Send a message from the front end. Check if the message was properly stored in the database.
10. If you haven’t already, add an ALB to the project.
11. Add a Route53 record and a CloudFront Distribution. Ask your DevOps Handler which R53 record you should use. Test your application again, this time through the hostname you configured through Route53.

TIPS: Research the following AWS services: Cloudfront, Route53, ALBs, NAT Gateway, Internet Gateway, ECS